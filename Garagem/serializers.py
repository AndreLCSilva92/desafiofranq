from rest_framework import serializers
from . models import Vagas

class PostarSerializerVagas(serializers.ModelSerializer):

   class Meta:
        fields = ('pessoa', 'nVaga', 'veiculo', 'nPlaca')
        model = Vagas