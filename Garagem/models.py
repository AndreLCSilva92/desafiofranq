from django.db import models
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from Pessoa.models import Pessoa_Cadastro
#from django.shortcuts import render
#from django.http import request

# Create your models here.

#def opcoes(request):
#    return render(request ,'garagem/opcoes.html')

class Vagas(models.Model):
    pessoa = models.OneToOneField(Pessoa_Cadastro, on_delete=CASCADE, primary_key=True)

    nVaga = models.CharField(max_length=4)
    veiculo = models.CharField(max_length=10)
    nPlaca = models.CharField(max_length=9)
    #opt = opcoes(request)

    def __str__(self):
        return self.nVaga


