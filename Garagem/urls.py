from django.urls import path
from . views import ListaVagas

urlpatterns = [
    path('vagas/', ListaVagas.as_view()),
]