from django.shortcuts import render
from rest_framework import generics, permissions
from . models import Vagas
from . serializers import PostarSerializerVagas

class ListaVagas(generics.ListAPIView):
    queryset = Vagas.objects.all()
    serializer_class = PostarSerializerVagas
    permission_classes = (permissions.IsAuthenticated,) # permissão de ver o conteúdo