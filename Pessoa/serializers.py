from rest_framework import serializers
from . models import Pessoa_Cadastro

class PostarSerializerPessoa(serializers.ModelSerializer):

   class Meta:
        fields = ('nome', 'telefone', 'email',)
        model = Pessoa_Cadastro