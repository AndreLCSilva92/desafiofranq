from django.urls import path
from . views import ListaPessoa

urlpatterns = [
    path('pessoa/', ListaPessoa.as_view()),
]