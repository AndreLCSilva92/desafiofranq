from django.shortcuts import render
from rest_framework import generics, permissions
from . models import Pessoa_Cadastro
from . serializers import PostarSerializerPessoa

# Create your views here.

class ListaPessoa(generics.ListCreateAPIView):
    queryset = Pessoa_Cadastro.objects.all()
    serializer_class = PostarSerializerPessoa
    permission_classes = (permissions.IsAuthenticated,) # permissão de ver o conteúdo