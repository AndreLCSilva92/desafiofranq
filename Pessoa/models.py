from django.db import models
from django.db.models.base import Model
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.

class Pessoa_Cadastro(models.Model):
    nome = models.CharField(max_length=20)
    telefone = models.CharField(max_length=15)
    email = models.CharField(max_length=30)
    usuario = models.OneToOneField(User, on_delete=CASCADE, primary_key=True)

    def __str__(self):
        return self.nome